package urlMonitoring;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class urlMonitoring {
	
	public static String URL1 = "http://management.diggit.com/login";
	public static String URL2 = "http://analytics.diggit.com";
	public static String URL3 = "http://opstats.diggit.com";
	public static String URL4 = "http://analytics.diggit.com/#/dashboard";
	public static String URL5 = "http://www.omdbapi.com/";
	public static String URL6 = "https://torrentz2.eu";
	public static String URL7 = "https://bitsnoop.com";
	public static String URL8 = "https://bitsnoop.com/api/latest_tz.php?t=all";
	public static String URL9 = "http://dx-torrentez.com";
	public static String URL10 = "http://export.diggit.com/#/jobs";
	public static String URL11 = "http://imdb.com";
	public static String URL12 = "http://google.com";
	
	
	public static String ChromeDriverServer = "C:\\YATINDRA\\Selenium\\Java\\SetUp\\chromedriver.exe";
	public static String outputlocation = "C://YATINDRA//Selenium//filename.txt";
	public WebDriver driver;
	
	
	@BeforeMethod
	public void BeforeMethod(){
		 System.setProperty("webdriver.chrome.driver", ChromeDriverServer);
         driver = new ChromeDriver();       
         driver.manage().window().maximize();
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}
	
	
	@Test
	public void url_1() throws Exception {
	  driver.get(URL1);
	  WebDriverWait wait = new WebDriverWait(driver, 15);
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("email")));
	  WebElement Email = driver.findElement(By.name("email"));
	  WebElement Password = driver.findElement(By.name("password"));
	  WebElement Login_btn = driver.findElement(By.xpath("//button[@type='submit']")); 
	  Email.sendKeys("yatindra@moogilu.com");
	  Password.sendKeys("123456");
	  Select role = new Select(driver.findElement(By.id("role_id")));
	  role.selectByVisibleText("Test Role");
	  long start = System.currentTimeMillis();
	  Login_btn.click();
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h3.ng-binding")));
	  String ManageText = driver.findElement(By.cssSelector("h3.ng-binding")).getText();
	  Assert.assertEquals("Management Dashboard", ManageText);
	  long finish = System.currentTimeMillis();
	  long totalTime = finish - start; 
	  System.out.println(URL1 + " --- " + "Total Time for Login - "+totalTime);
  }
  
	  @Test
	  public void url_2() throws Exception {
	  driver.get(URL2);
	  WebDriverWait wait = new WebDriverWait(driver, 15);
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button.btn.btn-primary")));
	  driver.findElement(By.cssSelector("button.btn.btn-primary")).click();
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("email")));
	  WebElement Email = driver.findElement(By.name("email"));
	  WebElement Password = driver.findElement(By.name("password"));
	  WebElement Login_btn = driver.findElement(By.xpath("//button[@type='submit']")); 
	  Email.sendKeys("yatindra@moogilu.com");
	  Password.sendKeys("123456");
	  Select role = new Select(driver.findElement(By.id("role_id")));
	  role.selectByVisibleText("Test Role");
	  Login_btn.click();
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h3.ng-binding")));
	  long start = System.currentTimeMillis();
	  driver.get(URL2);
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a.btn.btn-lg.btn-primary.ng-scope")));
	  long finish = System.currentTimeMillis();
	  long totalTime = finish - start; 
	  System.out.println(URL2 + " --- " + "Total Time for page load - "+totalTime);
	  String NewTitle = driver.findElement(By.cssSelector("a.btn.btn-lg.btn-primary.ng-scope")).getText();
	  Assert.assertEquals("Create New Titile", NewTitle);
	 	      
  }
	  
	  @Test
	  public void url_3() throws Exception {
		  driver.get(URL3);
		  WebDriverWait wait = new WebDriverWait(driver, 15);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Op. Stats")));
		  long start = System.currentTimeMillis();
		  driver.findElement(By.linkText("Op. Stats")).click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.panel-heading")));
		  long finish = System.currentTimeMillis();
		  long totalTime = finish - start; 
		  System.out.println(URL3 + " --- " + "Total Time for page load - "+totalTime);
		  		  
	  }
  	
	  @Test
	  public void url_4() throws Exception {
		  driver.get(URL2);
		  WebDriverWait wait = new WebDriverWait(driver, 15);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button.btn.btn-primary")));
		  driver.findElement(By.cssSelector("button.btn.btn-primary")).click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("email")));
		  WebElement Email = driver.findElement(By.name("email"));
		  WebElement Password = driver.findElement(By.name("password"));
		  WebElement Login_btn = driver.findElement(By.xpath("//button[@type='submit']")); 
		  Email.sendKeys("yatindra@moogilu.com");
		  Password.sendKeys("123456");
		  Select role = new Select(driver.findElement(By.id("role_id")));
		  role.selectByVisibleText("Test Role");
		  Login_btn.click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h3.ng-binding")));
		  driver.get(URL2);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a.btn.btn-lg.btn-primary.ng-scope")));
		  long start = System.currentTimeMillis();
		  driver.get(URL4);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.panel-heading.ng-binding")));
		  long finish = System.currentTimeMillis();
		  long totalTime = finish - start; 
		  System.out.println(URL4 + " --- " + "Total Time for page load - "+totalTime);
		  
	  }
	  
	  @Test
	  public void url_5() throws Exception {
		  long start = System.currentTimeMillis();
		  driver.get(URL5);
		  WebDriverWait wait = new WebDriverWait(driver, 15);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a.navbar-brand")));
		  long finish = System.currentTimeMillis();
		  long totalTime = finish - start; 
		  System.out.println(URL5 + " --- " + "Total Time for page load - "+totalTime);
		  		  
	  }
	  
	  @Test
	  public void url_6() throws Exception {
		  long start = System.currentTimeMillis();
		  driver.get(URL6);
		  WebDriverWait wait = new WebDriverWait(driver, 15);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input#thesearchbutton")));
		  long finish = System.currentTimeMillis();
		  long totalTime = finish - start; 
		  System.out.println(URL6 + " --- " + "Total Time for page load - "+totalTime);
	  }
	  
	  @Test
	  public void url_7() throws Exception {
		  long start = System.currentTimeMillis();
		  driver.get(URL7);
		  WebDriverWait wait = new WebDriverWait(driver, 15);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='submit']")));
		  long finish = System.currentTimeMillis();
		  long totalTime = finish - start; 
		  System.out.println(URL7 + " --- " + "Total Time for page load - "+totalTime);
	  }
	  
	  @Test
	  public void url_8() throws Exception {
		  long start = System.currentTimeMillis();
		  driver.get(URL8);
		  long finish = System.currentTimeMillis();
		  long totalTime = finish - start; 
		  System.out.println(URL8 + " --- " + "Total Time for page load - "+totalTime);
	  }
	  
	  @Test
	  public void url_9() throws Exception {
		  long start = System.currentTimeMillis();
		  driver.get(URL9);
		  WebDriverWait wait = new WebDriverWait(driver, 15);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.land-logo-text")));
		  long finish = System.currentTimeMillis();
		  long totalTime = finish - start; 
		  System.out.println(URL9 + " --- " + "Total Time for page load - "+totalTime);
	  }
	  
	  @Test
	  public void url_10() throws Exception {
		  long start = System.currentTimeMillis();
		  driver.get(URL10);
		  WebDriverWait wait = new WebDriverWait(driver, 15);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='box-title']")));
		  long finish = System.currentTimeMillis();
		  long totalTime = finish - start; 
		  System.out.println(URL10 + " --- " + "Total Time for page load - "+totalTime);
	  }
	  
	  @Test
	  public void url_11() throws Exception {
		  long start = System.currentTimeMillis();
		  driver.get(URL11);
		  WebDriverWait wait = new WebDriverWait(driver, 15);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='home_img']")));
		  long finish = System.currentTimeMillis();
		  long totalTime = finish - start; 
		  System.out.println(URL11 + " --- " + "Total Time for page load - "+totalTime);
	  	}
	  
	  @Test
	  public void url_12() throws Exception {
		  long start = System.currentTimeMillis();
		  driver.get(URL12);
		  WebDriverWait wait = new WebDriverWait(driver, 15);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//input[@type='submit']")));
		  long finish = System.currentTimeMillis();
		  long totalTime = finish - start; 
		  System.out.println(URL12 + " --- " + "Total Time for page load - "+totalTime);
	  }
	  
	  	  
  @AfterMethod
  public void Crash(){
	  driver.close();
	  driver.quit();
  }  
  }